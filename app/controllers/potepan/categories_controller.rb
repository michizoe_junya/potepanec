class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:root)
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.includes(master: %i[images default_price]).in_taxon(@taxon)
  end
end
