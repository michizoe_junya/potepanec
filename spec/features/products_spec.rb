require 'rails_helper'

RSpec.describe 'Products_feature', type: :feature do
  let(:product) do
    create(:product, name: 'Rails', price: '22.5')
  end

  it 'View show page' do
    visit potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIDBAG store"
    within '.lightSection' do
      expect(page).to have_content(product.name, count: 2)
    end

    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    expect(page).to have_link 'HOME', href: potepan_index_path
  end
end
