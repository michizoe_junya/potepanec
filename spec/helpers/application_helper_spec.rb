require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'application title helper' do
    it 'displayed full title' do
      expect(full_title('title')).to eq 'title - BIDBAG store'
      expect(full_title('')).to eq 'BIDBAG store'
      expect(full_title(nil)).to eq 'BIDBAG store'
    end
  end
end
